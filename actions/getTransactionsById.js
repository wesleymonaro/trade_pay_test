import getTransactionsById from '../services/getTransactionsById';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.get('/transactions/:transaction_id', async (req, res) => {
    try {
      const transaction_id = req.params.seller;
      const { data: response } = await getTransactionsById(transaction_id);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
