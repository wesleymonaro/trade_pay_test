import createBuyer from '../services/createBuyer';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.post('/buyer', async (req, res) => {
    try {
      const customer = req.body;
      const { data: response } = await createBuyer(customer);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
