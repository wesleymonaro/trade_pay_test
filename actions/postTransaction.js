import createTransaction from '../services/createTransaction';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.post('/transaction', async (req, res) => {
    try {
      const transaction = req.body;

      const { data: response } = await createTransaction(transaction);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
