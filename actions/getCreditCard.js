import getCreditCard from '../services/getCreditCard';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.get('/credit-card/:token_id', async (req, res) => {
    try {
      const token_id = req.params.token_id;
      const { data: response } = await getCreditCard(token_id);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
