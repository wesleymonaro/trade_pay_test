import createCreditCard from '../services/createCreditCard';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.post('/credit-card', async (req, res) => {
    try {
      const creditCard = req.body;
      const { data: response } = await createCreditCard(creditCard);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
