import getTransactionsBySeller from '../services/getTransactionsBySeller';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.get('/transactions/:seller', async (req, res) => {
    try {
      const seller = req.params.seller;
      const { data: response } = await getTransactionsBySeller(seller);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
