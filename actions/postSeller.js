import createSeller from '../services/createSeller';
/**
 * Rota da API
 * @param {*} router
 */
export default function (router) {
  router.post('/seller', async (req, res) => {
    try {
      const seller = req.body;

      const { data: response } = await createSeller(seller);
      return res.status(200).send(response);
    } catch (err) {
      console.trace(err);
      return res
        .status(err.response.status)
        .json({ error: err.response.statusText });
    }
  });
}
