import * as axios from 'axios';
import config from 'config';

const USER = config.get('billing.user');
const PASSWORD = config.get('billing.password');
const MARKETPLACE_ID = config.get('billing.marketplaceId');

const AUTHORIZATION = `Basic ${Buffer.from(USER + ':' + PASSWORD).toString(
  'base64'
)}`;
const BASE_URL = `https://api.zoop.ws/v1/marketplaces/${MARKETPLACE_ID}`;
const headers = {
  accept: 'application/json',
  'content-type': 'application/json',
  Authorization: AUTHORIZATION
};

module.exports = token_id => {
  return new Promise(async (resolve, reject) => {
    try {
      const { data: response } = await axios.get(
        `${BASE_URL}/tokens/${token_id}`,
        { headers }
      );

      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};
